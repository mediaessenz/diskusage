export const prettyBytes = (num) => {
  if (typeof num !== 'number' || Number.isNaN(num)) {
    throw new TypeError('Expected a number');
  }

  const neg = num < 0;
  const units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  let bytes = neg ? -num : num;

  if (bytes < 1) {
    return `${(neg ? '-' : '') + bytes} B`;
  }

  const exponent = Math.min(Math.floor(Math.log(bytes) / Math.log(1000)), units.length - 1);
  bytes = Number((bytes / (1000 ** exponent)).toFixed(2));
  const unit = units[exponent];

  return `${(neg ? '-' : '') + bytes} ${unit}`;
};
