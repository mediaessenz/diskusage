<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Diskusage\Controller;

use MEDIAESSENZ\Diskusage\Utility\SessionUtility;
use MEDIAESSENZ\Diskusage\Utility\SystemUtility;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Resource\Exception;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3Fluid\Fluid\View\ViewInterface;

class DiskusageController
{
    protected string $id = '';

    protected ?Folder $folderObject = null;

    /**
     * @var ModuleTemplate|null
     */
    protected ?ModuleTemplate $moduleTemplate = null;

    /**
     * @var ViewInterface|null
     */
    protected ?ViewInterface $view = null;

    public function __construct(
        protected readonly IconFactory $iconFactory,
        protected readonly ResourceFactory $resourceFactory,
        protected readonly ModuleTemplateFactory $moduleTemplateFactory,
        protected readonly ResponseFactoryInterface $responseFactory
    ) {
    }

    /**
     * @param ServerRequestInterface $request the current request
     *
     * @return ResponseInterface the response with the content
     * @throws InsufficientFolderAccessPermissionsException
     */
    public function handleRequest(ServerRequestInterface $request): ResponseInterface
    {
        $this->moduleTemplate = $this->moduleTemplateFactory->create($request);
        $parsedBody = $request->getParsedBody();
        $queryParams = $request->getQueryParams();

        if ((new Typo3Version())->getMajorVersion() < 12) {
            $this->id = $parsedBody['target'] ?? $queryParams['target'] ?? '';
            // create the folder object
            if ($this->id) {
                $this->folderObject = $this->resourceFactory->getFolderObjectFromCombinedIdentifier($this->id);
            }
        } else {
            $this->id = (string)($parsedBody['id'] ?? $queryParams['id'] ?? '');
            $this->setFolderObject();
        }

        $returnUrl = GeneralUtility::sanitizeLocalUrl($parsedBody['returnUrl'] ?? $queryParams['returnUrl'] ?? '');

        SessionUtility::getInstance()->set('combinedIdentifierOfCurrentFolder', $this->id);

        // Cleaning and checking target directory
        if (!$this->folderObject) {
            throw new RuntimeException(
                SystemUtility::getLanguageService()->sL('LLL:EXT:filelist/Resources/Private/Language/locallang_mod_file_list.xlf:paramError')
                . ': ' . SystemUtility::getLanguageService()->sL('LLL:EXT:filelist/Resources/Private/Language/locallang_mod_file_list.xlf:targetNoDir'),
                1294586845);
        }
        if ($this->folderObject->getStorage()->getUid() === 0) {
            throw new InsufficientFolderAccessPermissionsException(
                'You are not allowed to access folders outside your storages',
                1375889838
            );
        }

        $this->moduleTemplate->getDocHeaderComponent()->setMetaInformation([
            'combined_identifier' => $this->folderObject->getCombinedIdentifier(),
        ]);

        $buttonBar = $this->moduleTemplate->getDocHeaderComponent()->getButtonBar();

        // Back
        if ($returnUrl) {
            $backButton = $buttonBar->makeLinkButton()
                ->setHref($returnUrl)
                ->setTitle(SystemUtility::getLanguageService()->sL('LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:labels.goBack'))
                ->setIcon($this->iconFactory->getIcon('actions-view-go-back', Icon::SIZE_SMALL));
            $buttonBar->addButton($backButton, ButtonBar::BUTTON_POSITION_LEFT, 1);
        }

        // Rendering of the output via fluid
        $this->initializeView();

        return $this->moduleTemplate->renderResponse('BackendModule/Default');
    }

    protected function initializeView(): void
    {
        $this->view = GeneralUtility::makeInstance(StandaloneView::class);
        $this->view->setTemplatePathAndFilename(GeneralUtility::getFileAbsFileName(
            'EXT:diskusage/Resources/Private/Templates/BackendModule/Default.html'
        ));
    }

    /**
     * Generate a response by either the given $html or by rendering the module content.
     *
     * @param string $html
     * @return ResponseInterface
     */
    protected function htmlResponse(string $html): ResponseInterface
    {
        $response = $this->responseFactory
            ->createResponse()
            ->withHeader('Content-Type', 'text/html; charset=utf-8');

        $response->getBody()->write($html);
        return $response;
    }

    protected function setFolderObject(): void
    {
        $lang = $this->getLanguageService();
        $backendUser = $this->getBackendUser();

        try {
            if ($this->id !== '') {
                $backendUser->evaluateUserSpecificFileFilterSettings();
                $storage = GeneralUtility::makeInstance(StorageRepository::class)->findByCombinedIdentifier($this->id);
                if ($storage !== null) {
                    $identifier = substr($this->id, strpos($this->id, ':') + 1);
                    if (!$storage->hasFolder($identifier)) {
                        $identifier = $storage->getFolderIdentifierFromFileIdentifier($identifier);
                    }
                    $this->folderObject = $storage->getFolder($identifier);
                    // Disallow access to fallback storage 0
                    if ($storage->getUid() === 0) {
                        throw new InsufficientFolderAccessPermissionsException(
                            'You are not allowed to access files outside your storages',
                            1434539815
                        );
                    }
                    // Disallow the rendering of the processing folder (e.g. could be called manually)
                    if ($this->folderObject instanceof Folder && $storage->isProcessingFolder($this->folderObject)) {
                        $this->folderObject = $storage->getRootLevelFolder();
                    }
                }
            } else {
                // Take the first object of the first storage
                $fileStorages = $backendUser->getFileStorages();
                $fileStorage = reset($fileStorages);
                if ($fileStorage) {
                    $this->folderObject = $fileStorage->getRootLevelFolder();
                } else {
                    throw new \RuntimeException('Could not find any folder to be displayed.', 1349276894);
                }
            }

            if ($this->folderObject && !$this->folderObject->getStorage()->isWithinFileMountBoundaries($this->folderObject)) {
                throw new \RuntimeException('Folder not accessible.', 1430409089);
            }
        } catch (InsufficientFolderAccessPermissionsException $permissionException) {
            $this->folderObject = null;
            $this->addFlashMessage(
                sprintf($lang->sL('LLL:EXT:filelist/Resources/Private/Language/locallang_mod_file_list.xlf:missingFolderPermissionsMessage'), $this->id),
                $lang->sL('LLL:EXT:filelist/Resources/Private/Language/locallang_mod_file_list.xlf:missingFolderPermissionsTitle'),
                ContextualFeedbackSeverity::ERROR
            );
        } catch (Exception $fileException) {
            $this->folderObject = null;
            // Take the first object of the first storage
            $fileStorages = $backendUser->getFileStorages();
            $fileStorage = reset($fileStorages);
            if ($fileStorage instanceof ResourceStorage) {
                $this->folderObject = $fileStorage->getRootLevelFolder();
                if (!$fileStorage->isWithinFileMountBoundaries($this->folderObject)) {
                    $this->folderObject = null;
                }
            }
            if (!$this->folderObject) {
                $this->addFlashMessage(
                    sprintf($lang->sL('LLL:EXT:filelist/Resources/Private/Language/locallang_mod_file_list.xlf:folderNotFoundMessage'), $this->id),
                    $lang->sL('LLL:EXT:filelist/Resources/Private/Language/locallang_mod_file_list.xlf:folderNotFoundTitle'),
                    ContextualFeedbackSeverity::ERROR
                );
            }
        } catch (\RuntimeException $e) {
            $this->folderObject = null;
            $this->addFlashMessage(
                $e->getMessage() . ' (' . $e->getCode() . ')',
                $lang->sL('LLL:EXT:filelist/Resources/Private/Language/locallang_mod_file_list.xlf:folderNotFoundTitle'),
                ContextualFeedbackSeverity::ERROR
            );
        }

        if ($this->folderObject
            && !$this->folderObject->getStorage()->checkFolderActionPermission('read', $this->folderObject)
        ) {
            $this->folderObject = null;
        }

    }

    protected function addFlashMessage(string $message, string $title = '', ContextualFeedbackSeverity $severity = ContextualFeedbackSeverity::INFO): void
    {
        $flashMessage = GeneralUtility::makeInstance(FlashMessage::class, $message, $title, $severity, true);
        $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
        $defaultFlashMessageQueue = $flashMessageService->getMessageQueueByIdentifier();
        $defaultFlashMessageQueue->enqueue($flashMessage);
    }

    protected function getBackendUser(): BackendUserAuthentication
    {
        return $GLOBALS['BE_USER'];
    }

    protected function getLanguageService(): LanguageService
    {
        return $GLOBALS['LANG'];
    }
}
