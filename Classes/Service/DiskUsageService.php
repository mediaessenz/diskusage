<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Diskusage\Service;

use MEDIAESSENZ\Diskusage\Utility\FalUtility;
use TYPO3\CMS\Core\Resource\Folder;
use TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException;

class DiskUsageService
{
  /**
   * @param Folder $folder
   * @param bool $hideReferences
   *
   * @return array
   * @throws RouteNotFoundException
   */
  public static function getData($folder, $hideReferences = false): array
  {
    return FalUtility::dir2Array($folder, $hideReferences)['files'];
  }

}
