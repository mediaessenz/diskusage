<?php

use MEDIAESSENZ\Diskusage\Controller\BackendModuleAjaxController;

return [
    'diskusage_default' => [
        'path' => '/diskusage/default',
        'target' => BackendModuleAjaxController::class . '::defaultAction'
    ],
    'diskusage_hideReferencedFiles' => [
        'path' => '/diskusage/hidereferencedfiles',
        'target' => BackendModuleAjaxController::class . '::hideReferencedFilesAction'
    ],
];
