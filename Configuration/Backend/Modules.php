<?php
return [
    'extensions' => [
        'diskusage' => [
            'modules' => [
                'file_diskusage' => [
                    'parent' => 'file',
                    'position' => [],
                    'access' => 'admin',
                    'iconIdentifier' => 'extension-diskusage', // Icon-Identifier
                    'labels' => 'LLL:EXT:diskusage/Resources/Private/Language/locallang.xlf',
                    'navigationComponentId' => 'TYPO3/CMS/Backend/Tree/FileStorageTreeContainer',
                    'routes' => [
                        '_default' => [
                            'target' => \MEDIAESSENZ\Diskusage\Controller\BackendModuleController::class . '::defaultAction',
                        ],
                    ],
                ],
            ],
        ],
    ],
];
