﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.rst.txt

ChangeLog
---------

- v.1.0.0: Initial Release
- v.1.2.0: Make TYPO3 10 compatible; Update js libraries
- v.1.2.1: Fix typo in documentation
- v.3.0.0: Make TYPO3 12 compatible; Remove TYPO3 9/10 compatibility
- v.3.1.0: Switch to phpDocumentor
