﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: /Includes.rst.txt

What does it do?
^^^^^^^^^^^^^^^^

- This extension visualize the disk usage of TYPO3 storages using
  `Vue.js <https://vuejs.org/>`_ and `ECHARTS <https://echarts.apache.org/>`_

- It helps to find big, no more used files in a TYPO3 installation

- It brings its own backend module, but also integrates an additional
  button to the normal file list module

- It's currently only visible for admin users
