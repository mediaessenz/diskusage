﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: /Includes.rst.txt

Users manual
------------

- Install the extension from the TYPO3 Extension Repository or composer (package name: mediaessenz/diskusage)
- Click on the new backend module icon below the normal file list module
- Choose a storage folder to see disk usage of all underlying files (incl. subfolders)
- For files with references double click it to get detail infos
- For files without references click on it and choose "More Info" or "Delete"
- Restrict the result to only not referenced files by clicking on the corresponding button.
  This is useful to find no more needed big files quickly and easy
- Beside the folder tree, it is also possible to get deeper into a directory by double clicking the
  corresponding group header
- Be sure to update your reference index, before deleting any files!!!
- Also keep in mind, that some files maybe linked directly and therefore have no references!


Support my work
^^^^^^^^^^^^^^^

If you like my extension, I'd appreciate a small donation:

- `PayPal <https://www.paypal.com/de/cgi-bin/webscr?cmd=_send-money&nav=1&email=a.grein@mediaessenz.de>`_

- `PATREON <https://www.patreon.com/alexandergrein>`_

THANK YOU in advance!
