module.tx_diskusage {
  view {
    layoutRootPaths.10 = EXT:diskusage/Resources/Private/Layouts/BackendModule/
    templateRootPaths.10 = EXT:diskusage/Resources/Private/Templates/BackendModule/
    partialRootPaths.10 = EXT:diskusage/Resources/Private/Partials/BackendModule/
  }
}

